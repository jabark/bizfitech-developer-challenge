import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
  AccountsComponent,
  TransactionsComponent,
  UserComponent,
  UsersComponent,
} from './components';

const routes: Routes = [
  {
    path: 'users',
    component: UsersComponent,
  },
  {
    path: 'users/:userid',
    component: UserComponent,
  },
  {
    path: 'users/:userid/accounts',
    component: AccountsComponent,
  },
  {
    path: 'users/:userid/accounts/:accountid/transactions',
    component: TransactionsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
