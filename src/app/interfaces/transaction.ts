export interface Transaction {
  amount: number;
  description: string;
  timestamp: Date;
}
