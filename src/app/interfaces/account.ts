export interface Account {
  accountId?: number;
  accountName?: string;
  accountNumber: number;
  bank: string;
  balance?: number;
  id?: number;
}
