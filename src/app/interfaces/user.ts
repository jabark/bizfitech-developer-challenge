import { Account, Address } from './';

export interface User {
  id?: string;
  firstName: string;
  lastName: string;
  dob: string;
  address: Address;
  accounts?: Account[];
}
