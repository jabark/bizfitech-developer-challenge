import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of as ObservableOf } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Account, Transaction, User } from '../interfaces';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
};

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  private baseUrl = 'api/users';

  constructor(
    private http: HttpClient,
  ) { }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.baseUrl).pipe(
      catchError(this.handleError('getUsers', [])),
    );
  }

  getUser(userID: string): Observable<User> {
    const url = `${this.baseUrl}/${userID}`;
    return this.http.get<User>(url).pipe(
      catchError(this.handleError<User>(`getUser userID=${userID}`)),
    );
  }

  getUserAccounts(userID: string): Observable<Account[]> {
    const url = `${this.baseUrl}/${userID}/accounts`;
    return this.http.get<Account[]>(url).pipe(
      catchError(this.handleError<Account[]>(`getUserAccounts userID=${userID}`)),
    );
  }

  getUserAccount(userID: string, accountID: string): Observable<Account> {
    const url = `${this.baseUrl}/${userID}/accounts/${accountID}`;
    return this.http.get<Account>(url).pipe(
      catchError(this.handleError<Account>(`getUserAccount userID=${userID} accountID=${accountID}`)),
    );
  }

  getUserAccountTransactions(userID: string, accountID: string): Observable<Transaction[]> {
    const url = `${this.baseUrl}/${userID}/accounts/${accountID}/transactions`;
    return this.http.get<Transaction[]>(url).pipe(
      catchError(this.handleError<Transaction[]>(`getUserAccountTransactions userID=${userID} accountID=${accountID}`)),
    );
  }

  addUser(user: User): Observable<User> {
    return this.http.post<User>(this.baseUrl, user, httpOptions).pipe(
      catchError(this.handleError<User>('addUser')),
    );
  }

  deleteUser(user: User): Observable<User> {
    const id = user.id;
    const url = `${this.baseUrl}/${id}`;

    return this.http.delete<User>(url, httpOptions).pipe(
      catchError(this.handleError<User>('deleteUser')),
    );
  }

  updateUser(user: User): Observable<User> {
    const id = user.id;
    const url = `${this.baseUrl}/${id}`;

    return this.http.patch<User>(url, user, httpOptions).pipe(
      catchError(this.handleError<User>('updateUser')),
    );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return ObservableOf(result as T);
    };
  }
}
