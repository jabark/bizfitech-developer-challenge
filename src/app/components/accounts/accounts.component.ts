import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Account, User } from '../../interfaces';
import { UsersService } from '../../services';

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.scss'],
})
export class AccountsComponent implements OnInit {
  @Input() user: User;

  accounts: Account[];

  constructor(
    private route: ActivatedRoute,
    private usersService: UsersService,
  ) { }

  ngOnInit() {
    this.getAccounts();

    if (!this.user) {
      this.resetUser();
      this.getUser();
    }
  }

  getUser() {
    const userID = this.route.snapshot.paramMap.get('userid');
    this.usersService.getUser(userID).subscribe(
      (user) => {
        this.user = user;
      },
    );
  }

  getAccounts() {
    const userID = this.route.snapshot.paramMap.get('userid');
    this.usersService.getUserAccounts(userID).subscribe(
      (accounts) => {
        this.accounts = accounts;
      },
    );
  }

  private resetUser() {
    this.user = {
      firstName: '',
      lastName: '',
      dob: '',
      address: {
        addressLine1: '',
        addressLine2: '',
        postCode: '',
      },
    };
  }
}
