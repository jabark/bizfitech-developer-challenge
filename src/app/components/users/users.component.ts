import { Component, OnInit } from '@angular/core';
import { User } from '../../interfaces';
import { UsersService } from '../../services';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit {
  users: User[];
  newUser: User = {
    firstName: '',
    lastName: '',
    dob: '',
    address: {
      addressLine1: '',
      addressLine2: '',
      postCode: '',
    },
  };
  editUser: User = {
    firstName: '',
    lastName: '',
    dob: '',
    address: {
      addressLine1: '',
      addressLine2: '',
      postCode: '',
    },
  };
  isLoading = false;

  constructor(
    private usersService: UsersService,
  ) { }

  ngOnInit() {
    this.getUsers();
  }

  getUsers() {
    this.isLoading = true;
    this.usersService.getUsers().subscribe(
      (users) => {
        this.users = this.sortUsers(users);
        this.isLoading = false;
      },
    );
  }

  createUser() {
    const newUser: User = this.newUser;
    newUser.dob = new Date(this.newUser.dob).toISOString();
    this.usersService.addUser(newUser).subscribe(
      (user) => {
        this.users.push(user);
        this.users = this.sortUsers(this.users);
      },
    );
    this.resetNewUser();
  }

  cancelCreateUser() {
    this.resetNewUser();
    (document.getElementById('user-new') as HTMLInputElement).checked = false;
  }

  deleteUser(user: User) {
    const blnDelete = window.confirm(`Are you sure you want to delete the user "${user.firstName} ${user.lastName}"?`);
    if (blnDelete) {
      this.users = this.users.filter(u => u !== user);
      this.usersService.deleteUser(user).subscribe();
    }
  }

  updateEditUser(user?: User) {
    this.editUser = {...user};
  }

  cancelEditUser() {
    this.resetEditUser();
  }

  updateUser(user: User) {
    this.usersService.updateUser(user).subscribe(
      (newUser) => {
        this.users = this.users.filter(u => u.id !== newUser.id);
        this.users.push(newUser);
        this.users = this.sortUsers(this.users);
        this.resetEditUser();
      },
    );
  }

  private sortUsers(users) {
    return users.sort((a, b) => {
      return (a.firstName < b.firstName) ? -1 : (a.firstName > b.firstName) ? 1 : 0;
    }).sort((a, b) => {
      return (a.lastName < b.lastName) ? -1 : (a.lastName > b.lastName) ? 1 : 0;
    });
  }

  private resetNewUser() {
    this.newUser = {
      firstName: '',
      lastName: '',
      dob: '',
      address: {
        addressLine1: '',
        addressLine2: '',
        postCode: '',
      },
    };
  }

  private resetEditUser() {
    this.editUser = {
      firstName: '',
      lastName: '',
      dob: '',
      address: {
        addressLine1: '',
        addressLine2: '',
        postCode: '',
      },
    };
  }
}
