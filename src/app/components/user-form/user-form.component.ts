import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { User } from 'src/app/interfaces';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss'],
})
export class UserFormComponent implements OnInit {
  @Output() submitForm: EventEmitter<any> = new EventEmitter();
  @Output() cancel: EventEmitter<any> = new EventEmitter();
  @Input() user: User;

  constructor() { }

  ngOnInit() {
    this.user.dob = this.user.dob.split('T')[0];
  }

  onSubmitForm() {
    this.submitForm.emit();
  }

  onCancel() {
    this.cancel.emit();
  }
}
